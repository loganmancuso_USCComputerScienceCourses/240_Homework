#include "domino.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Class 'Domino' for a single domino.
 *
 * Author: Duncan A. Buell
 * Modified by Mancuso Logan
 * Date last modified: 27 September 2016
**/

/******************************************************************************
 * Constructor
**/
Domino::Domino() {
}

/******************************************************************************
 * Constructor with data supplied
**/
Domino::Domino(int left, int right) {
  this->left_ = left;
  this->right_ = right;
  this->dealt_ = false;
  this->played_ = false;
}

/******************************************************************************
 * Destructor
**/
Domino::~Domino() {
}

/******************************************************************************
 * Accessors and Mutators
**/

/******************************************************************************
 * Accessor 'GetLeft'.
 *
 * Returns:
 *   the left of the numbers on the domino 
**/
int Domino::GetLeft() const {
  return this->left_;
}

/******************************************************************************
 * Accessor 'GetRight'.
 *
 * Returns:
 *   the right of the numbers on the domino 
**/
int Domino::GetRight() const {
  return this->right_;
}

/******************************************************************************
 * Accessor 'HasBeenPlayed'.
 *
 * Returns:
 *   the boolean value of 'played_'
**/
bool Domino::HasBeenPlayed() const {
  if (this->played_ == true) {
    return true;
  }else {
    return false;
  }
}

/******************************************************************************
 * Accessor 'WasDealt'.
 *
 * Returns:
 *   the boolean value of 'dealt'
**/
bool Domino::WasDealt() const {
  if (this->dealt_ == true) {
    return true;
  }else {
    return false;
  }
}

/******************************************************************************
 * Mutator 'SetDealt'.
 *
 * Parameters:
 *   the boolean value to be set
**/
void Domino::SetDealt(bool value) {
  this->dealt_ = value;
}

/******************************************************************************
 * Mutator 'SetPlayed'.
 *
 * Parameters:
 *   the boolean value to be set
**/
void Domino::SetPlayed(bool value) {
  this->played_ = value;
}

/******************************************************************************
 * General functions.
**/
/******************************************************************************
 * Function 'FlipEnds' to flip a domino end for end.
**/
void Domino::FlipEnds() {
  int tmp = this->left_; //set left to temp
  this->left_ = this->right_; //set right to left
  this->right_ = tmp; //set right to temp
}

/******************************************************************************
 * Function 'ToString'.
 * prints in the form [left|right]
 * This returns the 'played_' as well as the left and right numbers.
**/
string Domino::ToString() const {
  string s;
  s = "[" + to_string(this->left_) + "|" + to_string(this->right_) + "]";
  return s;
}
