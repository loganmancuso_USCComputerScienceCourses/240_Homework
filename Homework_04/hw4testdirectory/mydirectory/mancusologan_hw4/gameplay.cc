#include "gameplay.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Class 'GamePlay' for playing part of a dominos game.
 *
 * Author: Duncan A. Buell
 * Modified by Mancuso Logan
 * Date last modified: 27 September 2016
**/

/******************************************************************************
 * Constructor
**/
GamePlay::GamePlay() {
}

/******************************************************************************
 * Destructor
**/
GamePlay::~GamePlay() {
}

/******************************************************************************
 * Accessors and Mutators
**/

/******************************************************************************
 * General functions.
**/

/******************************************************************************
 * Function 'DealDominos'.
 *
 * We run a loop to print out the first ten random numbers just to be able to
 * check that we are getting the same sequence of numbers.
 *
 * Then we loop until we get 'kHandsize' number of dominos.
 *
 *
 * Parameters:
 *   out_stream - the output stream to which to write
**/
void GamePlay::DealDominos(ofstream& out_stream, int seed) {
#ifdef EBUG
  Utils::log_stream << "enter DealDominos\n"; 
#endif

  MyRandom random = MyRandom(seed);
  int hand_size = 0; //number of dominos starting with
  while (hand_size < kHandsize) { //dominos should eventually be 12
    //random number 1 to 91
    int random_domino = random.RandomUniformInt(0,90);
    if (all_dominos_.at(random_domino).WasDealt() == false) {
      //has not been dealt so deal the domino
      my_dominos_.push_back(all_dominos_.at(random_domino));
      all_dominos_.at(random_domino).SetDealt(true);
      ++hand_size;
    }//end if
  }//end while
  //print the dominos in the hand
  string s = ToStringSeq("MY HAND", my_dominos_);
  out_stream << s << "\n" << endl;

#ifdef EBUG
  Utils::log_stream << "leave DealDominos\n"; 
#endif
}

/******************************************************************************
 * Function 'ExtendSeq'.
 * This function extends the sequence.
 *
 * Function takes in the parameters and then takes a domino at point i and
 * sets to compare that domino left and right sides
 * Taking the left and right perform the comparison if true then recursive call
 * else pop back one and try again with new domoino in seq
 * from this print the max length created and move to new starting domino
 *
 * Parameters:
 *   from - the number to be matched from unplayed dominos
 *   seq - the current sequence of played dominos
 *   level - the level of recursion on entry to this function
 *   out_stream - the output stream to which to write
**/
void GamePlay::ExtendSeq(int from, vector<Domino> seq, int level,
                         ofstream& out_stream) {
#ifdef EBUG
  Utils::log_stream << "enter extendSeq\n"; 
#endif

//  out_stream << "Number To Compare: " << from << endl;
//  out_stream << "Recursion Level: " << level << endl;
  
//  string f = ToStringSeq("Seq Pass From  \t",seq);
//  out_stream << f << endl;
  
//  string s = ToStringSeq("The Seq So Far \t", my_seq_);
//  out_stream << s << endl;
  
  int size = seq.size();
  for (int i = 0; i < size; ++i) {
    
    Domino to_compare = seq.at(i);
    int left = to_compare.GetLeft();
    int right = to_compare.GetRight();
    bool played = to_compare.HasBeenPlayed();
    
    if (played == false) { //has not been played
        
      if (from == left) {
        
//        out_stream << "Yes " << (i+1) << " " << from << " = " << left << endl;
        
        seq.at(i).SetPlayed(true);
        my_seq_.push_back(to_compare);
        string b = "New Seq Level ";
        b += to_string(level);
        string s = ToStringSeq(b, my_seq_);
        out_stream << s << endl;
        ++level;
        ExtendSeq(right,seq,level,out_stream);
       
      }//end if left
      else if (from == right) {
        
//        out_stream << "Yes " << (i+1) << " " << from << " = " << right << endl;
        
        seq.at(i).SetPlayed(true);
        to_compare.FlipEnds();
        my_seq_.push_back(to_compare);
        string b = "New Seq Level ";
        b += to_string(level);
        string s = ToStringSeq(b, my_seq_);
        out_stream << s << endl;
        ++level;
        ExtendSeq(left,seq,level,out_stream);
        
      }//end if right
      else {
        
//        out_stream << "No" << endl;
        
      }//end else

      int length = my_seq_.size();
      if (max_length_ < length) {
        
        max_length_ = length;
        max_seq_ = my_seq_;
        
      }//end if length
    }//end if played
  }//end for loop
  if (my_seq_.empty() == false) {
      my_seq_.pop_back();
  }

#ifdef EBUG
  Utils::log_stream << "leave ExtendSeq\n"; 
#endif
}

/******************************************************************************
 * Function 'FindLongestSeq'.
 * Funtion will have a loop to go through each staring number between 0 & 12
 * call the extend function and begin computation
 *
 * Parameters:
 *   out_stream - the output stream to which to write
**/
void GamePlay::FindLongestSeq(ofstream& out_stream) {

#ifdef EBUG
  Utils::log_stream << "enter FindLongestSeq\n"; 
#endif

  int level = 0; //recusion level
  int start_num = 0; //place value to start search in hand
  while (start_num <= kHandsize) {
    out_stream << "\nStarting Number: " << start_num << "\n" << endl;
    ExtendSeq(start_num, my_dominos_, level, out_stream);
    ++start_num;
    string q = "\nMax Seq Start Num ";
    q += to_string(start_num);
    string b = ToStringSeq(q, max_seq_);
    out_stream << b << endl;
    //clear variables to start over
    my_seq_.clear();
    max_seq_.clear();
    max_length_ = 0;
  }//end while

#ifdef EBUG
  Utils::log_stream << "leave FindLongestSeq\n"; 
#endif
}

/******************************************************************************
 * Function 'Initialize'.
**/
void GamePlay::Initialize() {
#ifdef EBUG
  Utils::log_stream << "enter Initialize\n"; 
#endif

  //initialize the all_dominos_ vector
  for (int left = 0; left <= kHandsize; ++left) {
    for (int right = left; right <= kHandsize; ++right) { // [left|(left-right)]
      Domino new_domino = Domino(left,right);
      all_dominos_.push_back(new_domino);
    }//end for j
  }//end for i

#ifdef EBUG
  Utils::log_stream << "leave initialize\n"; 
#endif
}

/******************************************************************************
 * Function 'ToString'.
 * This function dumps the 'vector' of all dominos to a 'string' and returns it.
 *
 * Returns:
 *   the 'ToString' of the data in the class
**/
string GamePlay::ToString() const {
#ifdef EBUG
  Utils::log_stream << "enter ToString\n"; 
#endif

  string s = "";
  int length = all_dominos_.size();
  for (int i = 0; i < length; ++i) {
    s += " ";
    s += all_dominos_.at(i).ToString();
  }

#ifdef EBUG
  Utils::log_stream << "leave ToString\n"; 
#endif
  return s;
}

/******************************************************************************
 * Function 'ToStringSeq'.
 * This function dumps the 'mySeq' data to a 'string' and returns it, with a
 * label for helping know what is being dumped.
 *
 * Returns:
 *   the 'ToString' of the 'vector' of dominoes passed in to the function
**/
string GamePlay::ToStringSeq(string label, const vector<Domino>& the_seq) const {
  vector<Domino>::const_iterator iter;

#ifdef EBUG
  Utils::log_stream << "enter ToStringSeq\n"; 
#endif
  string s;
  
  if (the_seq.empty() == true) {
    s = "";
  }
  else {
    s = label;
    s += ":";
    int length = the_seq.size();
    for (int i = 0; i < length; ++i) {
      s += " ";
      s += the_seq.at(i).ToString();
    }
  }//end else

#ifdef EBUG
  Utils::log_stream << "leave ToStringSeq\n"; 
#endif
  return s;
}
