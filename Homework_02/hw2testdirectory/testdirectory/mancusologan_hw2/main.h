/****************************************************************
 * Homework 2 header file.
 *
 * Author/copyright:  Duncan Buell
 * Used with permission and modified by: Logan Mancuso
 * Date: 25 August 2016
**/
#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <string>

#include "../../Utilities/utils.h"

using namespace std;

#endif // MAIN_H
