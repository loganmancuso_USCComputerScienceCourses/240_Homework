#include "record.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Class 'Record' for a single phone book record in a tree.
 *
 * Author: Duncan A. Buell
 * Used with permission and modified by: Mancuso Logan
 * Date last modified: 10 September 2016 
**/


/*
 * this program will do the following:
 *
 * establish getters and setters for instances of names and phone numbers
 * read the input string "input" from scanner in dothework.cc function readData
 * take the string and push to a stack at each .Next() in the string
 * using properties of a stack to pop the values back in reverse order to grab
 * all but the last value and the first two so as to store the other_name's
 * then store last element in forename and then assign the strings to record
 * the compare methods return an int value for each comparison >&=&<
 * the ToString method using a simple += it apends a string s with a space 
 * inbetween the variables
*/


static const string kTag = "Record: ";
//static vector<Record> record_data;

/******************************************************************************
 * Constructor
**/
Record::Record() {
}

/******************************************************************************
 * Destructor
**/
Record::~Record() {
}

/******************************************************************************
 * Accessors and Mutators
**/


string Record::GetSurname() const {
  return surname_;
}

string Record::GetPhoneNumber() const {
  return phone_number_;
}


/******************************************************************************
 * General functions.
**/

/******************************************************************************
 * Comparison function for 'phone_number_'
 *
 * Parameter:
 *   that - the 'Record' to compare 'this' against
 *
 * Returns:
 *   false if 'this' is less than 'that'
 *   false if 'this' is equal to 'that'
 *   true if 'this' is greater than 'that'
**/
bool Record::CompareNumber(Record that) {

  if ( phone_number_ <= that.phone_number_) {
//    cout << phone_number_ << "<=" << that.phone_number_ << endl;
    return false; //no swapp
  } else if ( phone_number_ > that.phone_number_ ) {
//    cout << phone_number_ << ">" << that.phone_number_ << endl;
    return true; // swapp
  } else {
    cout << "error in number comparison" << endl;
    return false;
  }

// return false; //error check rerturn value
}

/******************************************************************************
 * Comparison function for 'surname_'
 *
 * Parameter:
 *   that - the 'Record' to compare 'this' against
 *
 * Returns:
 *   true if 'this' Record 'surname_' is less than 'that' Record 'surname_'
**/
bool Record::CompareName(Record that) {

  if( surname_ <= that.surname_ ) {
//    cout << surname_ << "<=" << that.surname_ << endl;
    return false; //no swapp
  } else if( surname_ > that.surname_ ) {
//    cout << surname_ << ">" << that.surname_ << endl;
    return true; //swapp
  } else {
    cout << "error in name comparison" << endl;
    return false;
  }

// return false; //error check return value;
}

/******************************************************************************
 * Function 'ReadData'.
 *
 * We read the distinct lines of input data. Each line is a label
 * of the item, the number bought, and the cost per item.
 *
 * THERE IS NO BULLETPROOFING OF THE INPUT DATA.
 *
 * Parameter:
 *   data_stream - the opened 'Scanner' to read from
 *
 * take in string as constant, to safeguard accidental edits
 * push string at .Next() seperations onto stack to count
**/

Record Record::ReadData(const string& input) {
  stack<string> input_line;
//  vector<Record> record_data;
  ScanLine scanline;

#ifdef EBUG
  cout << kTag << "enter ReadData\n"; 
#endif
  Record record = Record();

  scanline.OpenString(input);
  while ( scanline.HasNext() ) {
    string word = scanline.Next();
    input_line.push(word);
  }//end while scanline.hasnext
  record.phone_number_ = input_line.top();
  input_line.pop(); //remove element
  record.surname_ = input_line.top();
  input_line.pop(); //remove element

  //to fill in other name if longer than 3
  while ( input_line.size() > 1 ) { //for names only 3 or more else ignored
    string part_of_other_name = record.other_name_;
    record.other_name_ = input_line.top();
    record.other_name_ += " "; //add space
    record.other_name_ += part_of_other_name;
    part_of_other_name = "";
    input_line.pop();
  }//end while input > 1
  record.forename_ = input_line.top();
  input_line.pop();

  //error in input or stack, evaluate
  if (input_line.size() > 0 ) { cout << "error in stack size" << endl; }

//error check values

/*
   cout << "string variable values " << forename
   << other_name << surname << phone_number << endl; //error check
*/

//error check

/*
  //print vector
  int size = record_data.size();
  for ( int i=0; i<size; i++ ) {
    string s = record_data[i].ToString();
    cout << "vector " << s << endl;
  }
*/

 return record;

#ifdef EBUG
  cout << kTag << "leave readData\n"; 
#endif

} // void Record::readData(Scanner& data_stream)

/******************************************************************************
 * Function 'ToString'.
 * This is the usual 'ToString' that returns the formatted data in the class.
**/
string Record::ToString() {
  string s = "";
  s += forename_ + " " + other_name_ + " ";
  s += surname_ + " " + phone_number_;
  return s;
} // void Record::ToString()
