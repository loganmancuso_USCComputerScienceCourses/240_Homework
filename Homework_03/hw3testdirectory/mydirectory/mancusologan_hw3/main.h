/****************************************************************
 * Rather generic header file.
 *
 * Author/copyright:  Duncan Buell
 * Used with permission and modified by: Mancuso Logan
 * Date last modified: 10 September 2016 
 *
**/

#ifndef MAIN_H
#define MAIN_H

#include <iostream>
using namespace std;

#include "../../Utilities/utils.h"
#include "../../Utilities/scanner.h"

#include "dothework.h"

#endif // MAIN_H
