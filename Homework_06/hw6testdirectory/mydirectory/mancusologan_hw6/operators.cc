#include "message.h"

/****************************************************************
 * Overloaded '<<' operator, not a 'Message' member function,
 * that calls the 'ToString' to output the message in a
 * formatted way.
 *
 * Parameters:
 *   out_stream: the output stream to which to send the output
 *   msg: the msg to be output
 *
 * Returns:
 *   the 'out_stream' with the formatted string form of the msg
**/
ostream& operator <<(ostream& out_stream, const Message& msg)
{
  out_stream << msg.ToString();
  return out_stream;
}
