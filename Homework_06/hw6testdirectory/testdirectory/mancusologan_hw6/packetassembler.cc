#include "packetassembler.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Class 'PacketAssembler' for assembling packets.
 *
 * Author: Duncan A. Buell
 * Date last modified: 8 August 2016
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
**/

/******************************************************************************
 * Constructor
**/
PacketAssembler::PacketAssembler() {
}

/******************************************************************************
 * Destructor
**/
PacketAssembler::~PacketAssembler() {
}

/******************************************************************************
 * Accessors and Mutators
**/

/******************************************************************************
 * General functions.
**/

/******************************************************************************
 * Function 'DumpMessage'.
 * iterator that runs through the map of messages and calls the message
 * ToString function that iterates through the message and returns a string
 * calls tostring function in message class
**/
string PacketAssembler::DumpMessage(int messageID) {
  string s = "";
  s += "Dump Message: ";
  s += to_string(messageID);
  for (auto it=messages_map_.begin(); it!=messages_map_.end(); ++it) {
    if (it->first == messageID) {
      s += it->second.ToString();
      s += "\n";
    }//end if
  }
  return s;
}

/******************************************************************************
 * Function 'MessagesMapContains'.
 * similar to duplicate packets, check messageID for duplicates
**/
bool PacketAssembler::MessagesMapContains(int messageID) const {
  bool return_value = false;
  for (auto it=messages_map_.begin(); it!=messages_map_.end(); ++it) {
    if (it->first == messageID) {
      return_value = true;
    }
  }
  return return_value;
}

/******************************************************************************
 * Function 'ReadPacket'.
 * reads the packets from the file and stores to a map with each new message
 * read from file 'scanner' and take line as string
 * push string to vector and pass vector to read packet function
 * once created, push to map, once map complete, print map
**/
void PacketAssembler::ReadPacket(string& input, ofstream& out_stream) {
  ScanLine scanline;
  //create vector for string storage
  vector<string> parse_line;
  scanline.OpenString(input);
  //read per line until empty
//  while (message.IsComplete() == false) {
  while (scanline.HasNext()) {
    string next_word = scanline.Next();
    parse_line.push_back(next_word);
  }//end while scanline
  parse_line.push_back(" ");//add space to end, padding shorter strings
  //send parse_line as vector to packet class
  Packet packet = Packet();
  packet.ReadPacket(parse_line);
//  out_stream << "Packet From File: " << packet.ToString() << endl;
  //message is in message map
//  out_stream << "Entering Computation" << endl;
  if (MessagesMapContains(packet.GetMessageID()) == true) {
//    out_stream << "message ID found in map" << endl;
    for (auto it=messages_map_.begin(); it!=messages_map_.end(); ++it) {
      if (it->first == packet.GetMessageID()) {
        if (it->second.Contains(packet) == false) {
          it->second.Insert(packet);
//          out_stream << "Packet added to message" << endl;
        }//end if
        else {
//          out_stream << "Packet Duplicate *************************" << endl;
        }
      }//end if
      if (it->second.IsComplete() == true) {
        out_stream << "Message Is Complete" << endl;
        out_stream << DumpMessage(it->first) << endl;
        messages_map_.erase(it->first);
      }//end if complete
    }//end for
  }//end if
  else {
//    out_stream << "New message ID, Creating Message" << endl;
    Message message = Message();
//    out_stream << "Inserting Packet Into Message" << endl;
    message.Insert(packet);
//    out_stream << "Inserting Message Into Map" << endl;
    messages_map_.insert( std::pair<int,Message>(message.GetMessageID(),message) );
  }//end else

}

/******************************************************************************
 * Function 'RunForever'.
 *
 * We simulate running forever.
**/
void PacketAssembler::RunForever(Scanner& scanner, ofstream& out_stream) {
  //file scanner read until empty
  while (scanner.HasNext()) {
    ScanLine scanline;
    //read line and store
    string this_line = scanner.NextLine();
    if(this_line == "") continue; //if line empty move on
    ReadPacket(this_line,out_stream);
  }//end while reading
}



