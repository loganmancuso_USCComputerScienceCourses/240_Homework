#include "message.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Class 'Message' for a single message.
 *
 * This class is essentially a container for a 'map' that holds the packets
 * for a message.
 *
 * Author: Duncan A. Buell
 * Date last modified: 29 October 2014
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
**/

/******************************************************************************
 * Constructor
**/
Message::Message() {
}

/******************************************************************************
 * Destructor
**/
Message::~Message() {
}

/******************************************************************************
 * Accessors and Mutators
**/

/******************************************************************************
 * Accessor 'GetMessageID'.
 *
 * Returns:
 *   the 'messageID'
**/
int Message::GetMessageID() const {
  return messageID_;
}

/******************************************************************************
 * General functions.
**/
/******************************************************************************
 * Method to test if a message contains a given packet.
 * a basic comparison function, iterate and compare
 * a duplicate is found based on the packet contained in the message
 * calling the Equals function in the packet class
**/
bool Message::Contains(const Packet& p) const {
  bool return_value = false;
  for (auto it=message_.begin(); it!=message_.end(); ++it) {
    if (it->second.Equals(p) == true) {
      return_value = true;
    }//end if
  }//end for
  return return_value;
}

/******************************************************************************
 * Method to insert a packet into a message.
 * using built in insert function for a map class
**/
void Message::Insert(const Packet& p) {
  message_.insert( std::pair<int,Packet>(p.GetPacketID(),p) );
  messageID_ = p.GetMessageID();
  packet_count_ = p.GetPacketCount();
}

/******************************************************************************
 * Method to test whether a message is complete.
 * iterate through the message comparing the number of packets to the
 * packet count stated by the packet
**/
bool Message::IsComplete() const {
  bool return_value = false;
  int numb_of_packets = message_.size();
  if (packet_count_ == numb_of_packets) {
    return_value = true;
  }//end if
  return return_value;
}

/******************************************************************************
 * Function 'ToString'.
 *
 * Returns:
 *   the 'ToString' of the message as a 'map' of packets
 * basic to string function, print a headder and call tostring of packet class
**/
string Message::ToString() const {
  string s = "\n";
  for (auto it=message_.begin(); it!=message_.end(); ++it) {
    s += it->second.ToString();
    s += " \n";
  }
  return s;
}
