/*************************************************************
 *'ReadMe.txt'
 *This is a generic ReadMe file
 *
 * Author/CopyRight: Mancuso, Logan
 *Last Edit Date: 12-01-2016--10:20:07
**/

/****************************************************************
 * Program 'Polling Simulation' Homework 7 
**/

Overview:
This program was originally written by Dr.Buell all original code
is used with his permission and should not be duplicated or 
replicated without his knowledge. Edits made to the files consist
of comments aimed at helping future programmers understand this 
code. No computational/operational changes have been made to this 
code.   

Files Included:
configuration.cc
configuration.h
dataallsorted.txt
hw7.pdf
main.cc
main.h
makefile
myrandom.cc
myrandom.h
onepct.cc
onepct.h
onevoter.cc
onevoter.h
README
simulation.cc
simulation.h
xconfig100zero.txt
xpctfile.txt
yylog
yyout

More Resources:
For more information on the code please see the System Guide
For information on operation and usage please see the User Manual


/****************************************************************
 * End 'README'
**/

