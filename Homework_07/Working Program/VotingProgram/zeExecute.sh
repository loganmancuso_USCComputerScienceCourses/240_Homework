# *************************************************************
# 'zeExecute.sh'
# this program will execute the Aprog in the test directory
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 12-01-2016--10:20:07
# *************************************************************

#!/bin/bash
echo "Descend into 'TestDirectory' directory"
cd TestDirectory
include=../../IOFiles
#
for item in *
do
  echo " "
  echo "EXECUTING" $item
  cd $item
  Aprog $include/config.txt $include/pct.txt $include/xout.txt $include/xlog.txt
  cd ..
echo "EXECUTION COMPLETE"
done
echo "Return from 'TestDirectory' directory"
cd ..
echo " "
# *************************************************************
# End 'zeExecute.sh'
# *************************************************************

